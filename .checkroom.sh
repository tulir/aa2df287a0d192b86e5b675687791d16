#!/bin/bash
if [[ -z "$HOMESERVER" ]]; then
	echo "Please set the HOMESERVER environment variable to the C-S API URL (e.g. https://matrix.org)"
	exit 1
elif [[ -z "$ACCESS_TOKEN" ]]; then
	echo "Please set the ACCESS_TOKEN environment variable to your access token"
	exit 1
elif [[ -z "$1" ]]; then
	echo "Usage: $0 <alias without #>"
	exit 1
fi
AUTH="Authorization: Bearer $ACCESS_TOKEN"
VERBOSE=${VERBOSE:-false}
FED_TESTER=${FED_TESTER:-"https://fed.mau.dev"}
FED_CHECK_DIR=${FED_CHECK_DIR:-"/tmp/fedchecks"}
mkdir -p "$FED_CHECK_DIR"

room_alias="%23$1"
if $VERBOSE; then
	echo "Room Alias: #$1"
fi
room_id=$(curl -s "$HOMESERVER/_matrix/client/r0/directory/room/$room_alias" | jq -r '.room_id')
echo "Room ID: $room_id"
members=$(curl -H "$AUTH" -s "$HOMESERVER/_matrix/client/r0/rooms/$room_id/joined_members" | jq -r '.joined | keys[]')
servers_dup=$(echo "$members" | sed -E 's/@[^:]+:(.+)/\1/')
servers=$(echo "$servers_dup" | sort -u)

declare -A member_counts
for member_server in $servers_dup; do
	((member_counts[$member_server]++))
done

errors=0
error_members=0
declare -A versions
declare -A version_members
declare -a old_members

member_count=$(echo "$members" | wc -l)
echo "$member_count members"
server_count=$(echo "$servers" | wc -l)
echo "$server_count servers"
echo

for server in $servers; do
	if [[ ! -f "$FED_CHECK_DIR/$server.json" ]]; then
		curl -s "$FED_TESTER/$server" -o "$FED_CHECK_DIR/$server.json"
	fi
	err=$(cat "$FED_CHECK_DIR/$server.json" | jq -r '.Version.error')
	if [[ "$err" != "null" ]]; then
		if $VERBOSE; then
			echo "$server does not work"
		fi
		((errors++))
		((error_members+=member_counts[$server]))
	else
		software=$(cat "$FED_CHECK_DIR/$server.json" | jq -r '.Version.name')
		version=$(cat "$FED_CHECK_DIR/$server.json" | jq -r '.Version.version')
		if $VERBOSE; then
			echo "$server is $software v$version"
		fi
		cut_version=$(echo "$version" | awk '{print $1;}')
		((versions[$cut_version]++))
		((version_members[$cut_version]+=member_counts[$server]))
		if [[ $cut_version != 1.* ]]; then
			old_members+=($(echo "$members" | grep $server))
		fi
	fi
done

echo "Summary:"
echo "  $errors servers servers with $error_members members did not work"
for version in "${!versions[@]}"; do
	echo "  ${versions[$version]} servers with ${version_members[$version]} members on $version" >> /tmp/matrix-room-server-versions
done
cat /tmp/matrix-room-server-versions | sort -n -r
rm -f /tmp/matrix-room-server-versions
echo "Outdated members: ${old_members[@]}"